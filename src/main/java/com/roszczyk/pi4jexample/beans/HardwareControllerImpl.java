package com.roszczyk.pi4jexample.beans;

import com.pi4j.io.gpio.*;
import com.roszczyk.pi4jexample.PiDemoApp;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@Component
public class HardwareControllerImpl implements HardwareController {

    private final GpioController gpioController;
    private final GpioPinPwmOutput pwmLedPin;
    private final GpioPinDigitalOutput motionLedPin;
//    private final GpioPinDigitalOutput statusLedPin;

    private final GpioPinDigitalOutput firstRelayPin;
    private final GpioPinDigitalOutput secondRelayPin;

    private final GpioPinDigitalInput sensorInput;
    private final Map<SystemLeds, GpioPinDigitalOutput> ledsMap;
    private final Map<Relays, GpioPinDigitalOutput> relayMap;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger( PiDemoApp.class );

    public HardwareControllerImpl() {
        ledsMap = new HashMap<>();
        relayMap = new HashMap<>();

        gpioController = GpioFactory.getInstance();

        motionLedPin = gpioController.provisionDigitalOutputPin( RaspiPin.GPIO_00, PinState.LOW );
        motionLedPin.setShutdownOptions( true, PinState.LOW, PinPullResistance.OFF );
        pwmLedPin = gpioController.provisionPwmOutputPin( RaspiPin.GPIO_01 );
        sensorInput = gpioController.provisionDigitalInputPin( RaspiPin.GPIO_02 );

//        statusLedPin = gpioController.provisionDigitalOutputPin( RaspiPin.GPIO_03, PinState.LOW );
//        statusLedPin.setShutdownOptions( true, PinState.LOW, PinPullResistance.OFF );

        firstRelayPin = gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_03, PinState.LOW);
        firstRelayPin.setShutdownOptions( true, PinState.LOW, PinPullResistance.OFF );

        secondRelayPin = gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_05, PinState.LOW);
        secondRelayPin.setShutdownOptions( true, PinState.LOW, PinPullResistance.OFF );

        ledsMap.put( SystemLeds.MOTION_LED, motionLedPin );
//        ledsMap.put( SystemLeds.STATUS_LED, statusLedPin );

        relayMap.put(Relays.FIRST_RELAY, firstRelayPin);
        relayMap.put(Relays.SECOND_RELAY, secondRelayPin);

        Runtime.getRuntime().addShutdownHook( new Thread() {
            @Override
            public void run() {
                LOGGER.info("Shutting down!");
                shutdown();
            }
        } );
    }

    public GpioPinDigitalInput getSensorInput() {
        return sensorInput;
    }

    public void shutdown() {
        gpioController.shutdown();
    }

    public void pulseShort( SystemLeds led ) {
        pulse( led, 150 );
    }

    public void pulseLong( SystemLeds led ) {
        pulse( led, 1000 );
    }

    public void on( SystemLeds led ) {
        ledsMap.get( led ).high();
    }

    public void off( SystemLeds led ) {
        ledsMap.get( led ).low();
    }

    public void setPwm( int level ) {
        pwmLedPin.setPwm( level );
    }

    @Override
    public void enableRelay() {
        relayMap.get(Relays.FIRST_RELAY).high();
        relayMap.get(Relays.SECOND_RELAY).high();
    }

    @Override
    public void disableRelay() {
        relayMap.get(Relays.FIRST_RELAY).low();
        relayMap.get(Relays.SECOND_RELAY).low();
    }


    private void pulse( SystemLeds led, long time ) {
        ledsMap.get( led ).pulse( time );
    }

}
