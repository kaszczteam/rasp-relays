package com.roszczyk.pi4jexample.beans;

/**
 * Created by kaszcz on 26.03.2017.
 */
public enum Relays {
    FIRST_RELAY,
    SECOND_RELAY
}
